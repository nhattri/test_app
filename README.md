# ENTRY TEST

This is an application that sums two operands using flutter

## Getting Started

First, you download the source code and import it to Android Studio or Visual Studio Code that supports flutter, now I will guide how to run code by Visual Studio Code

With Visual Studio Code, you click Run and Debug button

![Screenshot 2022-06-03 133246](https://user-images.githubusercontent.com/90992993/171800189-3cf3f4ec-057c-458e-a23d-ca9defe6c438.png)

Then, you choose an available device to run the application

![Screenshot 2022-06-03 133705](https://user-images.githubusercontent.com/90992993/171800609-5bcfd2a9-5f61-4b15-8b3f-5b9bc22165bd.png)

The application looks like this

![Screenshot 2022-06-03 134037](https://user-images.githubusercontent.com/90992993/171801062-a88de7fe-872b-40ff-94aa-fe87b743cb69.png)

![Screenshot 2022-06-03 134156](https://user-images.githubusercontent.com/90992993/171801447-6bfe38ac-3a17-4441-855b-3474af845f28.png)

If you do not insert one of the text field or string of alphabet it will show an error:
![image](https://user-images.githubusercontent.com/90992993/171801535-391459dd-050e-45b7-8646-c0ca2aefe58e.png)

This is logging debug mode which shows how to convert a string into a number:
![image](https://user-images.githubusercontent.com/90992993/171802152-86025f60-7b98-4986-895f-6e22f0c18d83.png)

It will show a message if they catch an alphabet:
![image](https://user-images.githubusercontent.com/90992993/171802439-5cde6844-e57a-4da2-9292-cd1a236f47c8.png)

