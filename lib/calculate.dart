import 'package:flutter/cupertino.dart';
import 'package:test_app/logging.dart';
import 'package:string_validator/string_validator.dart';

var log = logger(Calculate);

class Calculate extends ChangeNotifier {
  int charToNum(String obj) {
    int num = 0;
    int m = 1;
    for (int index = obj.length - 1; index >= 0; index--) {
      log.d(' number at index ${index} of $obj: ${obj[index]}');
      if (isAlpha(obj[index])) {
        log.e(' String has a alphabet character!');
        break;
      }
      num = num + m * int.parse(obj[index]);
      log.d(' number after parse char to int: ${num}');
      m = m * 10;
    }
    log.d(' RESULT AFTER PARSE: ${num}');
    print('----------------');
    return num;
  }

  int sumToString(String operand1, String operand2) {
    return charToNum(operand1) + charToNum(operand2);
  }
}
