import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app/logging.dart';
import 'calculate.dart';
import 'package:lottie/lottie.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'SourceSansPro',
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var formatter = NumberFormat.decimalPattern('en_us');
  TextEditingController _op1 = TextEditingController();
  TextEditingController _op2 = TextEditingController();
  int _result = 0;
  bool dirty = false;
  @override
  void dispose() {
    _op1.dispose();
    _op2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final log = logger(MyHomePage);
    final _formKey = GlobalKey<FormState>();
    final calculator = Calculate();
    return Scaffold(
      backgroundColor: const Color(0xffa7feea),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Text(
              'LET\'S DO \n SOME MATH!',
              style: TextStyle(fontSize: 30, color: Colors.black),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Container(
                height: 150,
                width: 150,
                child: Lottie.asset('assets/lottie/39420-calculator.json')),
            _TextInput(
              operand: _op1,
              element: 1,
            ),
            const Text(
              '+',
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            _TextInput(
              operand: _op2,
              element: 2,
            ),
            const Text(
              '=',
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            Visibility(
              visible: (dirty),
              child: Text(
                '${formatter.format(_result)}',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Container(
                width: 150,
                height: 50,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    side: BorderSide(
                      color: Colors.black,
                      width: 2.0,
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                    backgroundColor: Color(0xffe6c9a9),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      setState(() {
                        _result = calculator.sumToString(_op1.text, _op2.text);
                        log.d(
                            ' ========== FINAL RESULT: $_result ==============');
                        dirty = true;
                      });
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.calculate_outlined,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Calculate',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}

class _TextInput extends StatelessWidget {
  const _TextInput({
    Key? key,
    required TextEditingController operand,
    required this.element,
  })  : _operand = operand,
        super(key: key);

  final TextEditingController _operand;
  final int element;

  @override
  Widget build(BuildContext context) {
    final regex = RegExp('[0-9]');
    return Column(
      children: [
        const SizedBox(
          height: 7,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Container(
            height: 60,
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
                color: Color(0xfffff476),
                border: Border.all(width: 2, color: Colors.black),
                borderRadius: BorderRadius.circular(8.0)),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                cursorColor: Colors.black,
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      !regex.hasMatch(value)) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content:
                            Text('You have not inserted number ${element}!'),
                        backgroundColor: Colors.red,
                      ),
                    );
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: 'Enter operand ${element}',
                  border: InputBorder.none,
                ),
                textAlign: TextAlign.center,
                controller: _operand,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2!
                    .copyWith(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 7,
        ),
      ],
    );
  }
}
